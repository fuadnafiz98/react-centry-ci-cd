FROM node:lts-alpine3.13

RUN npm i webpack webpack-cli -g

WORKDIR /code

ADD yarn.lock .
ADD package.json .
RUN yarn install

ADD . .
RUN yarn run build
RUN du -ac build/
CMD ["yarn"  , "start"]
